#!/usr/bin/env python3
import os
import sys
import argparse
import h5py
import math
import numpy as np
from multiprocessing import Process, Manager

from rlbench.environment import Environment
from rlbench.action_modes import ActionMode
from rlbench.observation_config import ObservationConfig
from rlbench.task_environment import TaskEnvironmentError
from rlbench.backend.exceptions import BoundaryError
from rlbench.tasks import *  # Keep star import, allows arbitrary task str->class conversion

from rlbench_data_collection.util import ui_util, file_util


MIN_DEMO_DATA_POINTS = 5
RLBENCH_MODALITIES = [
    'joint_positions',
    'joint_velocities',
    'joint_forces',
    'gripper_open',
    'gripper_pose',
    'gripper_joint_positions',
    'gripper_touch_forces',
    'left_shoulder_rgb',
    'left_shoulder_depth',
    'left_shoulder_mask',
    'right_shoulder_rgb',
    'right_shoulder_depth',
    'right_shoulder_mask',
    'wrist_rgb',
    'wrist_depth',
    'wrist_mask',
    'front_rgb',
    'front_depth',
    'front_mask'
]

DATASET_MODALITIES = [
    'joint_positions',
    'joint_velocities',
    'joint_forces',
    'gripper_open',
    'gripper_position',
    'gripper_orientation',
    'gripper_joint_positions',
    'gripper_touch_forces',
    'left_shoulder_rgb',
    'left_shoulder_depth',
    'left_shoulder_mask',
    'right_shoulder_rgb',
    'right_shoulder_depth',
    'right_shoulder_mask',
    'wrist_rgb',
    'wrist_depth',
    'wrist_mask',
    'front_rgb',
    'front_depth',
    'front_mask'
]


def collect_demos(tasks, task_index, n_demos_per_task, max_task_attempts, task_lock,
                  h5_root, problem_tasks, headless):
    
    n_tasks = len(tasks)
    
    obs_config = ObservationConfig()
    obs_config.set_all(True)
    rlbench_env = Environment(action_mode=ActionMode(), obs_config=obs_config, headless=headless)
    rlbench_env.launch()

    task_env = None
    while True:
        # Figure out which task this process will collect
        with task_lock:
            # All processes can exit if all demos for all tasks have been collected
            if task_index.value >= n_tasks:
                break
            task = tasks[task_index.value]
            task_name = task.__name__
            # Update the index so the next process can take a new task
            task_index.value += 1
            
        task_env = rlbench_env.get_task(task)
        task_env.set_variation(np.random.randint(task_env.variation_count()))
        obs, descriptions = task_env.reset()

        path = os.path.join(h5_root, task_name)
        os.makedirs(path, exist_ok=True)
        
        file_idx = 1
        while file_idx <= n_demos_per_task:
            h5_filename = os.path.join(path, "{}_{:03d}.h5".format(task_name, file_idx))
            # Test if we already have a valid demo for this index, if so move on to next demo
            try:
                h5_file = h5py.File(h5_filename, 'a')
                if (len(h5_file) > 0
                    and all([k in h5_file for k in DATASET_MODALITIES])
                    and all([len(h5_file[k]) > MIN_DEMO_DATA_POINTS for k in DATASET_MODALITIES])):
                    file_idx += 1
                    continue
                else:
                    h5_file.close()
                    h5_file = h5py.File(h5_filename, 'w')
            except (OSError, KeyError) as e:
                h5_file.close()
                h5_file = h5py.File(h5_filename, 'w')

            demo = None
            n_attempts = 0
            while n_attempts < max_task_attempts and demo is None:
                try:
                    demo, = task_env.get_demos(1, live_demos=True)
                except (TaskEnvironmentError, BoundaryError) as e:
                    n_attempts += 1
                    
            if n_attempts == max_task_attempts:
                # Task didn't work, just move on to next task.
                problem_tasks.append(task_name)
                with task_lock:
                    task_index.value += 1
                break

            demo_data = {}
            for obs in demo:
                for attr in RLBENCH_MODALITIES:
                    if '_pose' in attr:
                        # Separate position and orientation so they can be processed independently
                        position_key = attr.replace('_pose', '_position')
                        orientation_key = attr.replace('_pose', '_orientation')
                        if position_key not in demo_data:
                            demo_data[position_key] = []
                        if orientation_key not in demo_data:
                            demo_data[orientation_key] = []
                        combined_data = np.expand_dims(getattr(obs, attr), 0)
                        demo_data[position_key].append(combined_data[:,:3])
                        demo_data[orientation_key].append(combined_data[:,3:])
                    else:
                        if attr not in demo_data:
                            demo_data[attr] = []
                        modality_data = np.expand_dims(getattr(obs, attr), 0)
                        demo_data[attr].append(modality_data)
            for key, data in demo_data.items():
                demo_data[key] = np.vstack(data)
                h5_file[key] = demo_data[key]
                
            file_idx += 1
            h5_file.close()
                    
        ui_util.print_info("\nCollected all {} demos for task {}".format(n_demos_per_task, task_name))
        
    rlbench_env.shutdown()

        
if __name__ == '__main__':
    # ARGS ======================================================================================
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--tasks', type=str, nargs='+',
                        help=("List of tasks to execute. Specify task names separated by "
                              "spaces, for example:\n  --tasks ReachTarget CloseBox"))
    parser.add_argument('--n_demos_per_task', type=int, default=10,
                        help="Number of demos to collect for each task.")
    parser.add_argument('--h5_root', type=str, required=True,
                        help="Root directory where recorded data will be stored.")
    parser.add_argument('--n_workers', type=int, default=1,
                        help="Number of processes to run (should max at number of cores)")
    parser.add_argument('--max_task_attempts', type=int, default=10,
                        help="Maximum number of attempts to make for each task before moving on.")
    parser.add_argument('--use_gui', action='store_true',
                        help="Run with GUI if true, run headless if false.")
    # -------------------------------------------------------------------------------------------
    args = parser.parse_args()
    # ===========================================================================================
    
    os.makedirs(args.h5_root, exist_ok=True)

    tasks = [globals()[t] for t in args.tasks] if args.tasks else FS95_V1['train'] + FS95_V1['test']

    manager = Manager()
    problem_tasks = manager.list()
    task_lock = manager.Lock()
    task_index = manager.Value('i', 0)

    if args.n_workers > 1 and args.use_gui:
        ui_util.print_warn("\nCannot use GUI when using multiple workers. Running headless.")
        headless = False
    else:
        headless = (not args.use_gui)
    
    process_args = (tasks, task_index, args.n_demos_per_task, args.max_task_attempts,
                    task_lock, args.h5_root, problem_tasks, headless)
    
    processes = [Process(target=collect_demos, args=process_args) for _ in range(args.n_workers)]
    [p.start() for p in processes]
    [p.join() for p in processes]

    ui_util.print_happy("\n\nData collection for all tasks complete.")
    ui_util.print_info("Files are in this location: {}\n\n".format(args.h5_root))

    if len(problem_tasks) > 0:
        print("\nData could not be recorded for these tasks:")
        for task in problem_tasks:
            print("  {}".format(task))
        print("\n")
