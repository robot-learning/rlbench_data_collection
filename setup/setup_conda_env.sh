#!/bin/bash
# ===========================================================================================
# This script installs CoppeliaSim, PyRep, RLBench, and the code in this repository into
# a conda virtual environment. It will install conda for you (if you don't already have it,
# and set everything up as a new environment called 'rlbench'.
#
# Supports both Ubuntu 16.04 (Kinetic) and Ubuntu 18.04 (Bionic). Only supports Python3.
# -------------------------------------------------------------------------------------------
# OPTIONS (set these as you wish below)
#
#   RLBENCH_RESOURCE_DIR: Directory where CoppeliaSim/PyRep/RLBench files are stored.
#   MINICONDA_DIR: Directory where Miniconda3 will be downloaded to.
#   WRITE_TO_BASHRC: Write conda and environment variables to ~/.bashrc if set to true.
# ===========================================================================================
RLBENCH_RESOURCE_DIR=$HOME/.rlbench_resources
MINICONDA_DIR=$HOME/.miniconda3
WRITE_TO_BASHRC=true

# ----------------------- You shouldn't need to edit below this line. -----------------------

SCRIPT_PATH=$(pwd)
mkdir -p $RLBENCH_RESOURCE_DIR

install_conda () {
    # Download and execute Miniconda installation script, creates Miniconda
    # environment in MINICONDA_DIR (variable should be set already).
    curl -O https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
    chmod +x Miniconda3-latest-Linux-x86_64.sh
    bash Miniconda3-latest-Linux-x86_64.sh -b -p $MINICONDA_DIR
    rm Miniconda3-latest-Linux-x86_64.sh
    
    source $MINICONDA_DIR/etc/profile.d/conda.sh
    conda init
}


# Figure out Ubuntu version
if [[ $(lsb_release -rs) == "18.04" ]]; then
    UBUNTU_VERSION="bionic"
elif [[ $(lsb_release -rs) == "16.04" ]]; then
    UBUNTU_VERSION="kinetic"
else
    echo "Unsupported version of Ubuntu installed: "
    lsb_release -rs
    exit 1
fi

# Try to install conda if it's not installed and user wants it, otherwise exit
CONDA_INSTALLED=false
if ! type "conda" &> /dev/null ; then
    echo -e "\nCould not find 'conda'."
    while true; do
        read -p "Do you wish to install and initialize conda ('yes' or 'no')? " yn
        case $yn in
            [Yy]* ) install_conda; break;;
            [Nn]* ) echo "Cannot continue installation without conda. Exiting."; exit 1;;
            * ) echo "Please answer 'yes' or 'no'.";;
        esac
    done
    CONDA_INSTALLED=true
    source $MINICONDA_DIR/etc/profile.d/conda.sh
else
    source $MINICONDA_DIR/etc/profile.d/conda.sh
    conda deactivate
    conda deactivate
fi


# Create the conda environment with installed dependencies
conda env remove -n rlbench
conda update -y -n base -c defaults conda
conda env create -f conda_env.yaml
conda activate rlbench

# Install this repo
cd $SCRIPT_PATH/..
python setup.py install

# CoppeliaSim
cd $RLBENCH_RESOURCE_DIR
if [ $UBUNTU_VERSION == "bionic" ] ; then
    COPPELIASIM_DIR="CoppeliaSim_Edu_V4_0_0_Ubuntu18_04"
elif [ $UBUNTU_VERSION == "kinetic" ] ; then
    COPPELIASIM_DIR="CoppeliaSim_Edu_V4_0_0_Ubuntu16_04"
fi
curl -O https://coppeliarobotics.com/files/$COPPELIASIM_DIR.tar.xz
echo -e "\nExpanding compressed file..."
tar xf $COPPELIASIM_DIR.tar.xz
rm $COPPELIASIM_DIR.tar.xz
mv $COPPELIASIM_DIR coppelia_sim

# There is an issue on Ubuntu 18.04 using conda that the video compression library
# dependencies are not resolved correctly: https://github.com/stepjam/PyRep/issues/142
# In order to fix this, we have to build their video recorder from source:
if [ $UBUNTU_VERSION == "bionic" ]; then
    cd coppelia_sim
    git clone https://github.com/CoppeliaRobotics/videoRecorder.git
    cd videoRecorder/vvcl
    mkdir build
    cd build
    cmake ..
    make
    cp libvvcl.so ../../../libvvcl.so
fi

if [ "$WRITE_TO_BASHRC" = true ] ; then
    echo '' >> $HOME/.bashrc
    echo '# RLBench variables' >> $HOME/.bashrc
    echo "export COPPELIASIM_ROOT=$RLBENCH_RESOURCE_DIR/coppelia_sim" >> $HOME/.bashrc
    echo 'export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$COPPELIASIM_ROOT' >> $HOME/.bashrc

    # The README for RLBench says to add this QT_QPA path, but it seems to ruin anything
    # else that uses QT5 (like pyqtgraph visualizations). Seems leaving it out doesn't
    # hurt anything and you can run both CoppeliaSim and other Qt stuff.
    
    # echo 'export QT_QPA_PLATFORM_PLUGIN_PATH=$COPPELIASIM_ROOT' >> $HOME/.bashrc
fi
export COPPELIASIM_ROOT=$RLBENCH_RESOURCE_DIR/coppelia_sim
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$COPPELIASIM_ROOT
# export QT_QPA_PLATFORM_PLUGIN_PATH=$COPPELIASIM_ROOT

# pyquaternion
cd $RLBENCH_RESOURCE_DIR
git clone https://github.com/KieranWynn/pyquaternion.git
cd pyquaternion
python setup.py install

# PyRep
cd $RLBENCH_RESOURCE_DIR
git clone https://github.com/stepjam/PyRep.git
cd PyRep
python setup.py install    

# RLBench
cd $RLBENCH_RESOURCE_DIR
git clone https://github.com/stepjam/RLBench.git
cd RLBench
python setup.py install


echo -e "\n----------------------------------------------------------------------------------"
echo -e "\n  COMPLETE. Run to activate environment:\n"
if [ "$CONDA_INSTALLED" = true ] ; then
    echo -e "      source ~/.bashrc"
fi
echo -e "      conda activate rlbench"
echo -e "\n  CHECK if your installation was successful:\n"
echo -e "      python test_rlbench_setup.py"
echo -e "\n----------------------------------------------------------------------------------\n"
