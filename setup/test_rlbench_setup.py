#!/usr/bin/env python3
import numpy as np
from rlbench.environment import Environment
from rlbench.action_modes import ArmActionMode, ActionMode
from rlbench.observation_config import ObservationConfig
from rlbench.tasks import ReachTarget

# ====================================================================================
# This is a test script to make sure your RLBench + PyRep + CoppeliaSim installation
# was successful. When you run this script you should see a CoppeliaSim GUI window
# come up, and the Panda robot will perform the ReachTarget task 3 times. You should
# then see output like this on the console:
#
#   RLBench test SUCCESS
#   
#   Observation data:
#     joint_positions: (1, 7)
#     joint_velocities: (1, 7)
#     joint_forces: (1, 7)
#     gripper_open: (1,)
#     gripper_pose: (1, 7)
#     gripper_joint_positions: (1, 2)
#     gripper_touch_forces: (1, 6)
#     left_shoulder_rgb: (1, 128, 128, 3)
#     left_shoulder_depth: (1, 128, 128)
#     left_shoulder_mask: (1, 128, 128)
#     right_shoulder_rgb: (1, 128, 128, 3)
#     right_shoulder_depth: (1, 128, 128)
#     right_shoulder_mask: (1, 128, 128)
#     wrist_rgb: (1, 128, 128, 3)
#     wrist_depth: (1, 128, 128)
#     wrist_mask: (1, 128, 128)
#
# If you don't see this, something went wrong in the installation and you should
# raise an issue on GitHub, especially if you were trying the installation script.
# =====================================================================================


RLBENCH_MODALITIES = [
    'joint_positions',
    'joint_velocities',
    'joint_forces',
    'gripper_open',
    'gripper_pose',
    'gripper_joint_positions',
    'gripper_touch_forces',
    'left_shoulder_rgb',
    'left_shoulder_depth',
    'left_shoulder_mask',
    'right_shoulder_rgb',
    'right_shoulder_depth',
    'right_shoulder_mask',
    'wrist_rgb',
    'wrist_depth',
    'wrist_mask'
]


if __name__ == '__main__':
    obs_config = ObservationConfig()
    obs_config.set_all(True)
    action_mode = ActionMode(ArmActionMode.ABS_JOINT_VELOCITY)
    env = Environment(action_mode, obs_config=obs_config)

    task = env.get_task(ReachTarget)
    demos = task.get_demos(3, live_demos=True)
    env.shutdown()

    print("\n\nRLBench test SUCCESS\n")
    print("Observation data:")
    observation = demos[0][0]
    for modality in RLBENCH_MODALITIES:
        data = np.expand_dims(getattr(observation, modality), 0)
        print("  {}: {}".format(modality, data.shape))
    print("\n\n")
