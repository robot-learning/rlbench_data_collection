import os
import h5py
import argparse
import numpy as np
from moviepy.editor import ImageSequenceClip

# This script converts RGB data recorded from demonstrations into GIF files for visualization.


if __name__ =='__main__':
    # ==========================================================================================
    parser = argparse.ArgumentParser()
    # Required ---------------------------------------------------------------------------------
    parser.add_argument('--h5_root', type=str, required=True,
                        help="Root directory where H5 folders are stored.")
    # Default ----------------------------------------------------------------------------------
    parser.add_argument('--fps', type=int, default=20, help="Frames per second for GIF.")
    parser.add_argument('--image_scale', type=int, default=1, help="Scale factor for image size.")
    parser.add_argument('--demo_index', type=int, default=1,
                        help="Demo index to use for all tasks.")
    parser.add_argument('--save_dir', type=str, default="~/Pictures/rlbench_gifs",
                        help="Directory where GIFs will be saved.")
    # ------------------------------------------------------------------------------------------
    args = parser.parse_args()
    # ==========================================================================================

    os.makedirs(args.save_dir, exist_ok=True)

    for task in [f for f in os.listdir(args.h5_root)
                 if os.path.isdir(os.path.join(args.h5_root, f))]:
        filename = os.path.join(args.h5_root,
                                "{}/{}_{:03d}.h5".format(task, task, args.demo_index))
        with h5py.File(filename, 'r') as h5_file:
            l_rgb = np.array(h5_file['left_shoulder_rgb']) * 255
            r_rgb = np.array(h5_file['right_shoulder_rgb']) * 255
            w_rgb = np.array(h5_file['wrist_rgb']) * 255
            f_rgb = np.array(h5_file['front_rgb']) * 255
            
            clip = ImageSequenceClip(list(l_rgb), fps=args.fps).resize(args.image_scale)
            filename = os.path.join(args.save_dir, "{}-left_shoulder_rgb.gif".format(task))
            clip.write_gif(filename, fps=args.fps)
            
            clip = ImageSequenceClip(list(r_rgb), fps=args.fps).resize(args.image_scale)
            filename = os.path.join(args.save_dir, "{}-right_shoulder_rgb.gif".format(task))
            clip.write_gif(filename, fps=args.fps)

            clip = ImageSequenceClip(list(w_rgb), fps=args.fps).resize(args.image_scale)
            filename = os.path.join(args.save_dir, "{}-wrist_rgb.gif".format(task))
            clip.write_gif(filename, fps=args.fps)

            clip = ImageSequenceClip(list(f_rgb), fps=args.fps).resize(args.image_scale)
            filename = os.path.join(args.save_dir, "{}-front_rgb.gif".format(task))
            clip.write_gif(filename, fps=args.fps)
