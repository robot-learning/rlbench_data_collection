#!/usr/bin/env python3
from rlbench.environment import Environment
from rlbench.action_modes import ArmActionMode, ActionMode
from rlbench.observation_config import ObservationConfig
from rlbench.tasks import ReachTarget


if __name__ == '__main__':
    obs_config = ObservationConfig()
    obs_config.set_all(True)
    action_mode = ActionMode(ArmActionMode.ABS_JOINT_VELOCITY)
    env = Environment(action_mode, obs_config=obs_config)

    task = env.get_task(ReachTarget)
    task.get_demos(3, live_demos=True)
    env.shutdown()
