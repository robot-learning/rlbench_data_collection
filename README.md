# RLBench Data Collection

This repository contains scripts for collecting data from [RLBench](https://github.com/stepjam/RLBench), a benchmarking framework based in [CoppeliaSim](http://www.coppeliarobotics.com/) (previously V-REP) for reinforcement learning and imitation learning.

A large dataset (~3TB) has been collected using this framework for the Franka Emika Panda robot and is publicly available [here](https://drive.google.com/drive/folders/1k7978z-TIA-c6V269jwMLU4cpItXMbYP?usp=sharing).

------------------------------------------------------------

## System Requirements
This package requires Python3. It has been tested only on Ubuntu 16.04 (Kinetic) and Ubuntu 18.04 (Bionic).

------------------------------------------------------------

## Installation
An install script [`setup/setup_conda_env.sh`](https://bitbucket.org/robot-learning/rlbench_data_collection/src/master/setup/setup_conda_env.sh) is provided in the root directory of this repo. It installs conda, creates a conda environment with all the necessary dependencies, and initializes everything. Check the script before running to see if the filepaths are where you want things to be saved. It doesn't do anything system-wide so if something goes awry, you can always just delete the directories it created and start over or go through things manually. If this script doesn't work for you, file an issue with the errors you get - it will need to be improved over time. Once the script runs, you can test to see if it worked by running

    source ~/.bashrc
    conda activate rlbench
    python test_rlbench_setup.py

You should see a CoppeliaSim GUI window come up and the Franka will perform the ReachTarget task 3 times. It should then exit and a success message will be printed to the console. If not, something went wrong and you should file an issue.

--------------------------------------------------------------------------------------

## Usage

The script `src/rlbench_data_collection/collect_data.py` implements the data collection functionality. It launches the RLBench environment and executes the specified tasks to collect the specified number of demos for each task. The data is then saved to HDF5 format files. The arguments to the script are as follows:

| Argument              | Type       | Default | Description                                                                                |
|-----------------------|------------|---------|--------------------------------------------------------------------------------------------|
| `--tasks`             | list (str) | -       | List of tasks to run data collection for. Space-separated, e.g. `-t ReachTarget CloseBox`. See the [RLBench repo](https://github.com/stepjam/RLBench/blob/master/rlbench/tasks/__init__.py) for a list of available tasks. |
| `--n_demos_per_task`  | int        | `100`   | Number of demos to collect for each task.                                                  |
| `--h5_root`           | str        | -       | Absolute path were data will be saved.                                                     |
| `--n_workers`         | int        | `1`     | Number of processes to run in parallel (maximum should be number of cores you have).       |
| `--max_task_attempts` | int        | `10`    | Maximum number of attempts to make for each task before moving on.                         | 

An example command might be:

```
python collect_data.py --tasks OpenBox CloseBox PutShoesInBox --n_demos_per_task 20 --h5_root /media/user/my_external_drive/my_rlbench_data_folder --n_workers 8 --max_task_attempts 5
```

--------------------------------------------------------------------------------------------------

## Data Format

The saved data for this example would have the structure:

```
|- /media/user/my_external_drive/my_rlbench_data_folder
    |- OpenBox
        |- OpenBox_001.h5
        |- OpenBox_002.h5
            ...
    |- CloseBox
        |- CloseBox_001.h5
        |- CloseBox_002.h5
            ...
    |- PutShoesInBox
        |- PutShoesInBox_001.h5
        |- PutShoesInBox_002.h5
           ...
```

Each H5 file stores one demo, which are all sensory modalities recorded over time. Each demo is a dictionary structure with modality name keys (see the [RLBench website](https://sites.google.com/view/rlbench) for more information on what the data is), and the values are the corresponding modality data over time. The current modalities are:

| Modality                  | Shape                        | Description                                                                                        |
|---------------------------|------------------------------|----------------------------------------------------------------------------------------------------|
| `joint_velocities`        | `(n_timesteps, 7)`           | 7-D vector of joint velocities.                                                                    |
| `joint_positions`         | `(n_timesteps, 7)`           | 7-D vector of joint positions.                                                                     |
| `joint_forces`            | `(n_timesteps, 7)`           | 7-D vector of joint torques.                                                                       |
| `gripper_open`            | `(n_timesteps, 1)`           | 1-D vector where boolean value 1=open and 0=closed.                                                |
| `gripper_position`        | `(n_timesteps, 3)`           | 3-D vector of end-effector position.                                                               |
| `gripper_orientation`     | `(n_timesteps, 4)`           | 4-D quaternion of end-effector orientation.                                                        |
| `gripper_joint_positions` | `(n_timesteps, 2)`           | 2-D vector of gripper finger positions along parallel gripper axis.                                |
| `gripper_touch_forces`    | `(n_timesteps, 6)`           | Force of each finger (3-D forces of one finger concatenated with 3-D forces of the second finger). |
| `left_shoulder_rgb`       | `(n_timesteps, 128, 128, 3)` | Left-shoudler camera RGB.                                                                          |
| `left_shoulder_depth`     | `(n_timesteps, 128, 128)`    | Left-shoudler camera depth.                                                                        |
| `left_shoulder_mask`      | `(n_timesteps, 128, 128, 3)` | Left-shoulder camera segmentation mask.                                                            |
| `right_shoulder_rgb`      | `(n_timesteps, 128, 128, 3)` | Right-shoulder camera RGB.                                                                         |
| `right_shoulder_depth`    | `(n_timesteps, 128, 128)`    | Right-shoulder camera depth.                                                                       |
| `right_shoulder_mask`     | `(n_timesteps, 128, 128, 3)` | Right-shoulder camera segmentation mask.                                                           |
| `wrist_rgb`               | `(n_timesteps, 128, 128, 3)` | Wrist camera RGB.                                                                                  |
| `wrist_depth`             | `(n_timesteps, 128, 128)`    | Wrist camera depth.                                                                                |
| `wrist_mask`              | `(n_timesteps, 128, 128, 3)` | Wrist camera segmentation mask.                                                                    |

You can easily load and use the recorded data using the `h5py` package (should have been installed when you ran the installation script, otherwise `conda install h5py`). The data is loaded as an `h5py.File` object and acts just like a dictionary, and the data can be easily converted to numpy arrays. For example:
```python
import h5py
import numpy as np

with h5py.File('OpenBox_001.h5', 'r') as h5_file:
    for modality_name, modality_data in h5_file.items():
        modality_data = np.array(modality_data)
        print("  {}: {}".format(modality_name, modality_data.shape))
```