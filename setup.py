from distutils.core import setup

setup(name='rlbench_data_collection',
      version='1.0',
      description='',
      author='Adam Conkey',
      author_email='adam.conkey@utah.edu',
      url='',
      packages=[
          'rlbench_data_collection',
          'rlbench_data_collection.util'
      ],
      package_dir={'': 'src'}
)
